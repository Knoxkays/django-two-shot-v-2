from django.contrib import admin
from receipts.models import Account, Receipt, ExpenseCategory


# Register your models here.
@admin.register(Account)
class Account(admin.ModelAdmin):
    list_display = [
        "name",
        "number",
    ]


@admin.register(Receipt)
class Receipt(admin.ModelAdmin):
    list_display = [
        "vendor",
        "total",
        "tax",
        "date",
    ]


@admin.register(ExpenseCategory)
class ExpenseCategory(admin.ModelAdmin):
    list_display = [
        "name",
    ]
